

# install files
pushd files/hotspot
find . -type f -exec install -Dm 644 {} "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/hotspot/"{} \;
chmod +x "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/hotspot/hotspot.sh"
popd

install -m 644 files/hotspot.service "${ROOTFS_DIR}/etc/systemd/system/hotspot.service"
sed -i "s|REPLACE_WITH_SCRIPTNAME|/home/${FIRST_USER_NAME}/hotspot/hotspot.sh|g" "${ROOTFS_DIR}/etc/systemd/system/hotspot.service"

on_chroot << EOF
systemctl unmask hostapd
systemctl disable hostapd
systemctl disable dnsmasq
systemctl enable hotspot
EOF