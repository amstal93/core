#!/bin/bash -e
find ./stage?/ -name "*.sh" -exec chmod +x {} \;
OIFS="$IFS"
IFS=$'\n'

# alarm function from https://unix.stackexchange.com/questions/1974/how-do-i-make-my-pc-speaker-beep
alarm() {
  ( \speaker-test --frequency $1 --test sine )&
  pid=$!
  \sleep ${2}s
  \kill -9 $pid
}

# confirm function from https://stackoverflow.com/questions/3231804/in-bash-how-to-add-are-you-sure-y-n-to-any-command-or-alias
confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

selected=false
while ! $selected; do
    if [[ -z $(lsblk -nSp -o NAME,SIZE,VENDOR,MODEL,TRAN | grep -v sata) ]]; then
        echo "No suitable devices to flash."
        break
    fi
    select block_device_str in $(lsblk -nSp -o NAME,SIZE,VENDOR,MODEL,TRAN | grep -v sata);
    do
        if [ -z "$block_device_str" ]; then
            continue;
        fi
        block_device=$(awk '{print $1}' <<< $block_device_str)
        echo "You picked $block_device."
        block_device_desc=$(ls ${block_device}?*)
        echo "This device contains the following partitions: $block_device_desc"
        if confirm; then
            selected=true
        fi
        break
    done
done
if $selected; then
    for partition in $block_device_desc  
    do 
    echo "Unmounting $partition"
        umount -l "$partition" || true
    done 
    if [ -z $skip_build ]; then
        IFS="$OIFS"
        sudo ./build.sh
        IFS=$'\n'
    fi
    #in case it got mounted again
    for partition in $block_device_desc
    do 
        echo "Unmounting $partition"
        umount -l "$partition" || true
    done
    image_file=$(cat deploy/latest)
    if [ ! -z "$image_file" ]; then
        unzip -p "deploy/image_$image_file" | sudo dd bs=4M of="$block_device" status=progress conv=fsync
        echo "Success!"
        
        for i in {1..10}; do
            (alarm 600 0.8 > /dev/null 2>&1)
            sleep 0.4s
        done
    fi
fi


IFS="$OIFS"
