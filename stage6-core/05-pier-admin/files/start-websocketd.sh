#!/bin/bash -e

# start the websocketd server
TRC_FILE=/var/log/websocketd.log
[ -f $TRC_FILE ] && tar czf $TRC_FILE.tgz $TRC_FILE && rm -f $TRC_FILE
# download latest list of authorized commands from pier-admin project
# TODOS: replace develop with master for production
AUTH_CMDS_URL="https://gitlab.com/pierhost/pier-admin/-/raw/develop/authorized_commands"
wget -O REPLACE_WITH_WSD_DIRECTORY/authorized_commands $AUTH_CMDS_URL
pushd REPLACE_WITH_WSD_DIRECTORY
./websocketd --port 8088 ./websocket-server.py > $TRC_FILE 2>&1
popd
