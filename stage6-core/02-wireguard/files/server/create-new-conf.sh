#!/bin/bash -e


# Set the needed variables
pushd /etc/wireguard/
user_id=$(awk '{ print 1+$1 }' counter_file)
if [[ $user_id -gt 255 ]]; then
    echo "You have reached the maximum number of clients supported."
    echo "To add more clients manually, change the server configuration in /etc/wireguard/wg0.conf to a bigger subnet and manually configure your additional clients."
else
    printf "%s" "$user_id" > counter_file
    echo "Creating client's Wireguard key pair for user $user_id"
    client_private_key=$(wg genkey) # the keys are base64 encoded, padded with =
    client_public_key=$(wg pubkey <<< "$client_private_key")

    cat peer.conf |\
    sed "s&client_private_key&${client_private_key}&g" |\
    sed "s&user_id&${user_id}&g"                       
    # Update the server's conf
    cat peer-fragment.conf | \
    sed "s&client_public_key&${client_public_key}&g" | \
    sed "s&user_id&${user_id}&g" >> wg0.conf
    systemctl restart wg-quick@wg0
fi
popd
