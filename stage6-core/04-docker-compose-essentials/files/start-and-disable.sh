#!/bin/bash -e

# Update pier-services from the gitlab repo (using current branch)
# and generate the docker-compose files

cd REPLACE_WITH_SERVICES_DIRECTORY
# starts with empty list of user services
echo "services:" > config.yml
# update pier services to last version on current branch (set in stage 7)
pushd pier-services
git pull || echo "Failed to pull. Continuing..."
popd
# download latest list of authorized commands from pier-admin project
# TODOS: replace develop with master for production
AUTH_CMDS_URL="https://gitlab.com/pierhost/pier-admin/-/raw/develop/authorized_commands"
wget -O REPLACE_WITH_ADMIN_DIRECTORY/authorized_commands $AUTH_CMDS_URL || echo "Failed to download latest authorized commands. Continuing..."

./pier-services/docker-compose-generator.py

# create docker networks for critical services
docker network create --driver=bridge --attachable --internal=false traefik-network
docker network create --driver=bridge --attachable --internal=false db-network

# Then start the 3 service stacks in the configuration specified at generation time
for d in critical admin user
do
    pushd Generated/$d
    docker-compose up -d --remove-orphans
    popd
    # wait for previous stack to be started before running next one
    # Usefull e.g. if a user service needs the db to be started 
    sleep 10 
done

# WIP: figure out the current device and router IP addresses which can be used whatever the DOMAIN is started or not.
function getIpVars() {
    ip route list default | while read default via routerIP dev interface proto protocol src deviceIP rest;
    do 
        echo routerIP=$routerIP; 
        echo deviceIP=$deviceIP;
        echo ipInterface=$interface
    done
}
eval $(getIpVars)

systemctl disable docker-services-startup
