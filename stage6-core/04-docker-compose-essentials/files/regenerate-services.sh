#!/bin/bash -e

# WARNING: for admin stack: if needed to use a non standard image tag or gitlab repo instead of dockerhub
# set PIER_TAG or DOCKER_REGISTRY in ~/Services/.env before running this script 

# regenerate only given stack (or user if none given)
STACK=${1:-user}

[ $STACK == "user" ] || [ $STACK == "admin" ] || [ $STACK == "critical" ] || (echo "usage: $0 [critical|admin|user] (default=user)" && exit 1)

cd REPLACE_WITH_SERVICES_DIRECTORY

if [ $STACK != "admin" ];
then 
    pushd Generated/$STACK
    docker-compose down
    popd
else 
    FORCE_RECREATE=--force-recreate
    # download latest list of authorized commands from pier-admin project
    # TODOS: replace develop with master for production
    AUTH_CMDS_URL="https://gitlab.com/pierhost/pier-admin/-/raw/develop/authorized_commands"
    wget -O /home/pier/Admin/websocketd/authorized_commands $AUTH_CMDS_URL
fi

# Update pier-services from the gitlab repo
# Warning: branch should be set first
pushd pier-services
git pull
popd

./pier-services/docker-compose-generator.py --$STACK  

# force rescan db in case of new database creation before restarting services
[ $STACK == "user" ] && docker exec db /usr/local/bin/check-create-db.sh

pushd Generated/$STACK
# need to explicitely pull for updating existing images
docker-compose pull
docker-compose up -d $FORCE_RECREATE
# clean old images and containers
docker container prune -f
docker image prune -f
popd


# restart websocketd-server
# /!\ breaks the connection to the API container! Must be done at the very last moment.
[ $STACK == "admin" ] && (sleep 1; systemctl restart websocketd.service)&

