#!/bin/bash

pushd REPLACE_WITH_SERVICES_DIRECTORY

pushd Generated
docker-compose stop
popd

dirname="Backup/$(date +%Y-%m-%dT%R)"
mkdir -p ${dirname}
#todo: update labels on rebranding
for vol in $(docker volume ls -q -f label=pier); do
	if docker volume ls -q -f label=pier.nobackup | grep -w -q ${vol}; then
		echo ${vol}
		docker run --rm -v ${vol}:/volume -v `pwd`/${dirname}:/backup ubuntu bash -c "cd /volume && tar czf /backup/${vol}.tar.gz ."
	fi
done

pushd Generated
docker-compose start
popd

popd