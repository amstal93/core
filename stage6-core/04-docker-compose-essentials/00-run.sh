#!/bin/bash -e

# ======= Services generation and startup ========
# set up service to launch services on startup
install -m 644 files/docker-services-startup.service "${ROOTFS_DIR}/etc/systemd/system/docker-services-startup.service"

# install the starting script
mkdir -p "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/"
install -m  744 files/start-and-disable.sh "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/start-and-disable.sh"
sed -i "s|REPLACE_WITH_SCRIPTNAME|/home/${FIRST_USER_NAME}/Services/start-and-disable.sh|g" "${ROOTFS_DIR}/etc/systemd/system/docker-services-startup.service"
# install the regeneration script
install -m  744 files/regenerate-services.sh "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/regenerate-services.sh"

# install backup and restore scripts
install -m  744 files/create_backup.sh "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/create_backup.sh"
install -m  744 files/restore_from_backup.sh "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/restore_from_backup.sh"


# ======= Docker environment file ========
install -m  600 files/.env "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/.env"

sed -i "s|REPLACE_WITH_DEVICE_DNS|${DEVICE_DNS}|" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/.env"
if [[ -n "${CERT_EMAIL}" ]]
then
    sed -i "s|ex@example.com|${CERT_EMAIL}|" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/.env"
else
    log "Warning: No E-Mail address for Let's Encrypt provided. Using ex@example.com instead."
fi
sed -i "s|REPLACE_WITH_MX_DOMAIN|${MX_DOMAIN}|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/.env"
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|/home/${FIRST_USER_NAME}/Services|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/.env"



# adjust services directories in scripts
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|/home/${FIRST_USER_NAME}/Services/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/start-and-disable.sh"
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|/home/${FIRST_USER_NAME}/Services/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/regenerate-services.sh"
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|/home/${FIRST_USER_NAME}/Services/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/create_backup.sh"
sed -i "s|REPLACE_WITH_SERVICES_DIRECTORY|/home/${FIRST_USER_NAME}/Services/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/restore_from_backup.sh"
# adjust admin directories in scripts
sed -i "s|REPLACE_WITH_ADMIN_DIRECTORY|/home/${FIRST_USER_NAME}/Admin/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/start-and-disable.sh"
sed -i "s|REPLACE_WITH_ADMIN_DIRECTORY|/home/${FIRST_USER_NAME}/Admin/|g" "${ROOTFS_DIR}/home/${FIRST_USER_NAME}/Services/regenerate-services.sh"


# enable service
on_chroot << EOF
FU_UID=\$(id -u ${FIRST_USER_NAME})
FU_GID=\$(id -g ${FIRST_USER_NAME})
sed -i "s|REPLACE_WITH_FIRST_USER_UID|${FU_UID}|g" "/home/${FIRST_USER_NAME}/Services/.env"
sed -i "s|REPLACE_WITH_FIRST_USER_GID|${FU_GID}|g" "/home/${FIRST_USER_NAME}/Services/.env"
systemctl enable docker-services-startup
EOF
